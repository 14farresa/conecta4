public class Adria implements Jugador
{
	public Adria()
	{
	}

	public int moviment(Tauler t, int color)
	{
		float alfa = Float.MAX_VALUE;
		float beta = Float.MIN_VALUE;
		//return alfaBeta(t, 6, alfa, beta, 1);
	}

	public String nom()
	{
		return "Adria";
	}

	private float alfaBeta(Tauler t, int prof, float alfa, float beta, int jugador)
	{
		if (prof == 0 || !t.espotmoure())
			return valorar(t);

		float v;
		if (jugador == 1) {
			v = Float.MIN_VALUE;
			for (int i = 0; i < 8; i++) {
				Tauler tau = new Tauler(t);
				if (tau.solucio(i, jugador))
					return 100F;
				v = Math.max(v, alfaBeta(tau, prof-1, alfa, beta, -1));
				alfa = Math.max(alfa, v);
				if (beta <= alfa)
					break;
			}
		} else {
			v = Float.MAX_VALUE;
			for (int i = 0; i < 8; i++) {
				Tauler tau = new Tauler(t);
				if (tau.solucio(i, jugador))
					return -100F;
				v = Math.min(v, alfaBeta(tau, prof-1, alfa, beta, 1));
				alfa = Math.min(beta, v);
				if (beta <= alfa)
					break;
			}
		}
		return v;
	}

	private float valorar(Tauler t)
	{
		return 0F;
	}
}
